import processing.serial.*;

// The serial port:
Serial myPort;

void setup() {
  // List all the available serial ports:
  println(Serial.list());
  // Open the port you are using at the rate you want:
  myPort = new Serial(this, Serial.list()[6], 9600);
}

void draw() {
  while (myPort.available() > 0) {
    //int inByte = myPort.read();
    String inString = myPort.readString();
    println(inString);
  }
}
