#include <SoftwareSerial.h>
 
// NOTE ABOUT USING DIGITAL PINS AS RX ON ARDUINO MEGA 
// Digging around in the source code of the library, I came accross the following:

// Specifically for the Arduino Mega 2560 (or 1280 on the original Arduino Mega)
// A majority of the pins are NOT PCINTs, SO BE WARNED (i.e. you cannot use them as receive pins)
// Only pins available for RECEIVE (TRANSMIT can be on any pin):
// (I've deliberately left out pin mapping to the Hardware USARTs - seems senseless to me)
// Pins: 10, 11, 12, 13,  50, 51, 52, 53,  62, 63, 64, 65, 66, 67, 68, 69

// So pins 2 & 3 won't work (for receive).  Maybe this post can help someone else that has the same problem. 
 
//int resetPin = 12; 

// Counters & Flags
unsigned long count = 0;
unsigned long beginTime = 0;
int checkReader = -1;
int readingReader = -1;
int foundTagIndex = -1;

#define DEBUG false
#define DEBUG_BEGIN if (DEBUG){
#define DEBUG_END }

// Tags
#define NUM_TAGS 11
#define TAG_LENGTH 12
char tagString[TAG_LENGTH];
int tagPosIndex = 0;
String tags[NUM_TAGS] = {
 "4400E6F5FCAB",
 "4F00C3163BA1",
 "4C0034C95CED",
 "4C003525DE82",
 "4F00C3056AE3",
 "4F00C302A628",
 "4F00C331239E",
 "4C0034E1B32A",
 "4C0034D6DA74",
 "4C0034F48E02",
 "4C0034F48E02"
};

// Readers
#define NUM_READERS 10
#define STOP_ID 20
int readers[NUM_READERS][2] = {
 {10,9},
 {11,8},
 {12,7},
 {13,6},
 {50,40},
 {51,41},
 {52,42},
 {53,43},
 {19,29},
 {17,27}
};
SoftwareSerial serials[NUM_READERS] = { 
SoftwareSerial(10, 9),
SoftwareSerial(11, 8),
SoftwareSerial(12, 7),
SoftwareSerial(13, 6),
SoftwareSerial(50, 40),
SoftwareSerial(51, 41),
SoftwareSerial(52, 42),
SoftwareSerial(53, 43),
SoftwareSerial(19, 29),
SoftwareSerial(17, 27)
};
unsigned long resetTimes[NUM_READERS];
int curReaderIndex = -1;
unsigned long nextResetAt = 1;
int positions[NUM_READERS];
int fails[NUM_READERS];

// Reset
#define RESET_DURATION 140
#define TAG_READ_GRACE_PERIOD 10
unsigned long resetStartedAt = 0;

void setup(){
  Serial.begin(9600);
  //pinMode(rxPin, INPUT);
  //pinMode(resetPin, OUTPUT);
  //digitalWrite(resetPin, HIGH);
  // Software Serial
  for( int i = 0; i < NUM_READERS; i++){
    resetTimes[i] = 0;
    pinMode(readers[i][1], OUTPUT);
    positions[i] = -1;
    fails[i] = 0;
    if (readers[i][0] != 19 && readers[i][0] != 17){
      pinMode(readers[i][0], INPUT);
      serials[i].begin(9600);
    }
  }
  Serial1.begin(9600);
  Serial2.begin(9600);
  serials[0].listen();
}

void loop(){
  if (checkReader > -1){
    if (checkReader != 8 && checkReader != 9){
      // Software seial
      if (!serials[checkReader].isListening()){
        serials[checkReader].listen();
      } else {
        //Serial.println("Is Listening"); 
      }
      if (serials[checkReader].available()>0){
        //Serial.println("Available");
        int c = serials[checkReader].read();
        handleByte(c);
      }
    } else {
      if (checkReader == 8){
        while (Serial1.available()) {
          int readByte = Serial1.read();
          handleByte(readByte);
        }
      } else if (checkReader == 9) {
        while (Serial2.available()) {
          int readByte = Serial2.read();
          handleByte(readByte);
        }  
      }
    } 
  }
  if (nextResetAt > 0 && millis() >= nextResetAt){
    startNextResetSequence();
  }
  resetReaders();
}

void handleByte(int readByte){
  //Serial.println(readByte);
  if(readByte == 2){
    DEBUG_BEGIN
    Serial.print("\nBegining\n");
    Serial.print("After reset begin: ");
    Serial.print(millis()-resetStartedAt);
    Serial.print(" ms\n");
    DEBUG_END
    readingReader = checkReader;
    beginTime = millis();
    tagPosIndex = 0;
    //Serial.println("");
  }
  if(readByte == 3){
    //Serial.println("");
    readingReader = -1;
    checkReader = -1;
    //Serial.print("end\n");
  }
  if(readingReader > -1 && readByte != 2 && readByte != 10 && readByte != 13 && tagPosIndex < TAG_LENGTH){
      char character = (char)readByte;
      DEBUG_BEGIN
      Serial.print(character);
      DEBUG_END
      //Serial.print("|");
      tagString[tagPosIndex] = (char)readByte;
      tagPosIndex++;
      if (tagPosIndex==TAG_LENGTH){
        tagReadComplete();
      }
  }
}
void tagReadComplete(){
  DEBUG_BEGIN
  Serial.print("\nTag Read Complete: ");
  Serial.print((millis()-beginTime));
  Serial.print(" ms\n");
  DEBUG_END
//  Serial.print( "-" );
//  for (int i = 0; i < strlen(tagString); i++){
//    Serial.print( tagString[i] );
//  }
//  Serial.print( "-" );
//  Serial.print("\n");
  int tagIndex = getTagIndex( tagString );
  if (tagIndex > -1){
    tagIndexFound(tagIndex, readingReader);
  } else {
    DEBUG_BEGIN
    Serial.print("Tag index not found: ");
    Serial.print( "-" );
    for (int i = 0; i < strlen(tagString); i++){
      Serial.print( tagString[i] );
    }
    Serial.print( "-" );
    DEBUG_END
  }
  readingReader = -1;
  DEBUG_BEGIN Serial.print("\n"); DEBUG_END
  clearTag();
  nextResetNow();
}

void nextResetNow(){
  resetTimes[checkReader] = 0;
  readingReader = -1;
  checkReader = -1;
  nextResetAt = millis() + 10;
}

void tagIndexFound(int index, int readerIndex){
  DEBUG_BEGIN
  Serial.print("Tag Index Found: ");
  Serial.print(index);
  Serial.print("\nOn reader: ");
  Serial.print(readerIndex);
  Serial.print("\n");
  DEBUG_END
  foundTagIndex = index;
  if (positions[readerIndex] != index){
    // New ID on reader 
    DEBUG_BEGIN Serial.print("NEW ID ON READER\n"); DEBUG_END
    Serial.print(readerIndex);
    Serial.print(" ");
    Serial.print(index);
    Serial.println();
  }
  positions[readerIndex] = index;
  fails[readerIndex] = 0;
}

void tagLeftReader(int tagIndex, int readerIndex){
  fails[readerIndex] = 0;
  positions[readerIndex] = -1;
  DEBUG_BEGIN Serial.print("TAG REMOVED FROM REAER\n"); DEBUG_END
  Serial.print(STOP_ID);
  Serial.print(" ");
  Serial.print(tagIndex);
  Serial.println();
}

void startNextResetSequence(){
  nextResetAt = 0;
  boolean oneMoreChance = false;
  if (checkReader > -1 && foundTagIndex == -1 ){
     if (positions[checkReader] != -1){
       if (fails[checkReader] > 0){
         // Tag off reader
         tagLeftReader(positions[checkReader], checkReader);
       } else {
         // One more chance then fail
         DEBUG_BEGIN Serial.print("ONE MORE CHANCE\n"); DEBUG_END
         fails[checkReader] += 1;
         oneMoreChance = true;
       }
     }
  }
  if (oneMoreChance == false){
    curReaderIndex = curReaderIndex+1;
  }
  if (curReaderIndex >= NUM_READERS) curReaderIndex = 0;
  DEBUG_BEGIN
  Serial.print( "\nResseting: " );
  Serial.print( curReaderIndex );
  DEBUG_END
  resetTimes[curReaderIndex] = millis();
  nextResetAt = millis() + RESET_DURATION + TAG_READ_GRACE_PERIOD;
  checkReader = -1;
}
void resetReaders(){
  for (int i = 0; i < NUM_READERS; i++){
    resetReader(i);
  }  
}
void resetReader(int readerIndex){
  unsigned long milli = millis();
  if (readerIndex > -1 && resetTimes[readerIndex] > 0 && milli - resetTimes[readerIndex] <= RESET_DURATION){
    // Start reset
    if (checkReader != readerIndex){
      // FIrst time only
      checkReader = readerIndex;
      DEBUG_BEGIN
      Serial.print("\nSetting checkReader = ");
      Serial.print(checkReader);
      Serial.print("\n");
      DEBUG_END
      foundTagIndex = -1;
      resetStartedAt = milli;
    }  
    digitalWrite(readers[readerIndex][1], HIGH);
  } else if (readerIndex > -1 && resetTimes[readerIndex] > 0 && milli - resetTimes[readerIndex] > RESET_DURATION) {
    // Reset ended
    unsigned long newMillis = millis();
    resetTimes[readerIndex] = 0;
    DEBUG_BEGIN
    Serial.print("\nReset Finished for reader: ");
    Serial.print(readerIndex);
    Serial.print("\n");
    DEBUG_END
  } else {
    digitalWrite(readers[readerIndex][1], LOW);
  }
}


void clearTag(){
///////////////////////////////////
//clear the char array by filling with null - ASCII 0
//Will think same tag has been read otherwise
///////////////////////////////////
  for(int i = 0; i < strlen(tagString); i++){
    tagString[i] = 0;
  }
}

boolean compareTag(char one[], char two[]){
  //Serial.println(strlen(one));
  //Serial.println(strlen(two));
  if(strlen(one) == 0) return false; //empty

  for(int i = 0; i < TAG_LENGTH; i++){
    if(one[i] != two[i]) return false;
  }

  return true; //no mismatches
}

int getTagIndex( char tag[] ){
  for( int i = 0; i < NUM_TAGS; i++){
    char tempChar[TAG_LENGTH];
    tags[i].toCharArray(tempChar, TAG_LENGTH+1);
//    Serial.print( "-" );
//    for (int i = 0; i < strlen(tempChar); i++){
//      Serial.print( tempChar[i] );
//    }
//    Serial.print( "-\n" );
    if ( compareTag( tag, tempChar ) ){
      return i; 
    }
  }
  return -1;
}

